﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SimpleFinalize
{
    class Program
    {
        class MyResourceWrapper
        {
            ~MyResourceWrapper()
            {
                for (int i = 0; i < 1000; i++)
                    Console.Write(".");
                Console.ReadLine();
            }
        }

        static void Main(string[] args)
        {
            MyResourceWrapper rw = new MyResourceWrapper();
            // при релизе GC сработал автоматом?
            GC.Collect();
            //rw = null;
            Console.WriteLine("****Fun with finaizers****/n");
            Console.WriteLine("Hit the return key to shut down this app");
            Console.WriteLine("and force the gc to invoke finalize()");
            Console.WriteLine("for finalizeble objects created in this AppDomain");

            Console.ReadLine();
        }
    }
}
