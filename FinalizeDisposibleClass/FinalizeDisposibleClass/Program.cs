﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Threading;

namespace FinalizeDisposibleClass
{
    class ResourceWrapper : IDisposable
    {
        private bool disposed = false;

        public void Dispose()
        {
            CleanUp(true);
            Console.Beep();
            GC.SuppressFinalize(this);
        }
        ~ResourceWrapper()
        {
            Console.WriteLine("Destructed!!!");
            Console.Beep();
            CleanUp(false);
        }

        private void CleanUp(bool clean)
        {
            if(!this.disposed)
            {
                if(clean)
                {
                    Console.WriteLine("Освобождение ресурсов ");
                    for (int i = 0; i < 5; i++)
                        Console.Write("FREE\n");
                }
                Console.WriteLine("Finalize");
                Console.Beep();
            }
            disposed = true;
        }

    }
    class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine("**** Dispose() / Destructor Combo Platter ****");
            var rw = new ResourceWrapper();
            Console.WriteLine(rw);
            Console.WriteLine(new string('-', 20));

            rw.Dispose();
            Thread.Sleep(4000);
            rw.Dispose();
            Thread.Sleep(4000);
            rw.Dispose();
            Thread.Sleep(4000);
            rw.Dispose();

            ResourceWrapper rw2 = new ResourceWrapper();
            rw.Dispose();
            Console.ReadLine();
        }
    }
}
