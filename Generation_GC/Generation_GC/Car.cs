﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Generation_GC
{
    class Car
    {
        private int speed;
        private string name;

        public Car(String name, int speed)
        {
            this.name = name;
            this.speed = speed;
        }

        public override string ToString()
        {
            return string.Format("{0} едет со скоростью {1} км в час", name, speed);
        }
    }
}
