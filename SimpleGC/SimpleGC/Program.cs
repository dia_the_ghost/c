﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SimpleGC
{

    class Test {
	int [] arr = new int[10000000];
	public void method(int i)
	{
		Console.WriteLine(i);
	}
	~Test()
	{
		Console.WriteLine("Object" + this.GetHashCode() + " deleted");
	}

    class Program
    {
        static void Main(string[] args)
        {
            var tests = new Test[10000];
		
		try
		{
			for(int i = 0; i < tests.Length; i++)
			{
                //из-за того что на объект нет ссылки и он просто создался, 
                //гарбидж коллектор считает это мусором и уничтожает это
				//Test test = new Test();
				//test.method(i);
                tests[i] = new Test();
                tests[i].method(i);
			}
		}
		catch(OutOfMemoryException ex)
		{
			Console.ForegroundColor = ConsoleColor.Red;
			Console.WriteLine(ex.Message);
			Console.WriteLine("Управляемая куча переполнена");
			Console.ForegroundColor = ConsoleColor.Gray;
		}
		Console.ReadLine();
	}
        }
    }
}
